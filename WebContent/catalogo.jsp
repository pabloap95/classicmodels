<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	
	<% 
	String pag = request.getParameter("pag");
	int pagina = 0;
	if(pag!=null){
		pagina=Integer.parseInt(pag); 
	}
	
	%>
<sql:query var="modelos" dataSource="jdbc/classicmodels">
 select productCode, productName, productScale, productDescription, MSRP from products LIMIT 12 OFFSET <% out.println(0+(12*pagina)); %>;
</sql:query>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Catálogo de Productos</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"></script>
</head>
<body>
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark mb-4">
    <div class="container">
      <a class="navbar-brand" href="catalogo.jsp">Catálogo</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
      <ul class="navbar-nav ml-auto">
      <c:choose>
      <c:when test="${not empty sessionScope.customer}">
        
          <li class="nav-item active">
            <a class="nav-link" href="#">${sessionScope.customer}</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="cart.jsp">Carrito</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="logout">Cerrar sesion</a>
          </li>
        
        </c:when>
        <c:otherwise>
        	<li class="nav-item">
           		<a class="nav-link" href="login.jsp">Iniciar Sesion</a>
          	</li>
          	<li class="nav-item">
            	<a class="nav-link" href="registro.jsp">Registrarse</a>
          	</li>
		</c:otherwise>		
       </c:choose>
       </ul>
      </div>
    </div>
  	</nav>
	<div class="container-fluid mx-auto row">
	<c:forEach var="modelo" items="${modelos.rows}">
	<div class="col-lg-4 col-md-6 mb-4">
		<div class="card h-100 bg-info text-white">
		    <div class="card-header text-center bg-dark">Código producto: ${modelo.productCode}</div>
			<div class="card-body">
			<h3 class="card-title text-center">${modelo.productName}</h3>
			<p class="card-text text-center">Escala ${modelo.productScale}</p>
			<p class="text-center text-dark"><b>${modelo.MSRP} €</b></p>
			<p class="card-text text-justify">${modelo.productDescription}</p>
			</div>		
			<c:if test="${not empty sessionScope.customer}">
				<div class="card-footer text-center">
					<a href="cart?c=${modelo.productCode}&n=${modelo.productName}&p=${modelo.MSRP}" class="btn btn-danger">Añadir al carrito</a>
				</div>
			</c:if>		
		</div>
	</div>
	</c:forEach>
	</div>
	<div class="d-flex justify-content-center my-2">
		<div class="btn-group" role="group" aria-label="Basic example">
		<% if(pagina>0){%>
		  <a href="catalogo.jsp?pag=<% out.println(pagina - 1); %>" class="btn btn-primary text-white mr-2">Anterior</a>
		 <%} %>
		  <a href="catalogo.jsp?pag=<% out.println(pagina + 1); %>" class="btn btn-primary text-white">Siguiente</a>
		</div>
	</div>
	<footer class="py-3 bg-dark">
    <div class="container">
      <p class="m-0 text-center text-white">Despliegue de aplicaciones Web 2ºDAW</p>
    </div>
  	</footer>
</body>
</html>
