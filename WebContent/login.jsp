<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" session="true"%>
<c:if test="${not empty sessionScope.customer} ">
	<c:redirect url="catalogo.jsp" />
</c:if>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Inicio de Sesión</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"></script>
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark mb-4">
    <div class="container">
      <a class="navbar-brand" href="catalogo.jsp">Catálogo</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
      <ul class="navbar-nav ml-auto">
      <c:choose>
      <c:when test="${not empty sessionScope.customer}">
        
          <li class="nav-item active">
            <a class="nav-link" href="#">${sessionScope.customer}</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="cart.jsp">Carrito</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="logout">Cerrar sesion</a>
          </li>
        
        </c:when>
        <c:otherwise>
        	<li class="nav-item">
           		<a class="nav-link" href="login.jsp">Iniciar Sesion</a>
          	</li>
          	<li class="nav-item">
            	<a class="nav-link" href="registro.jsp">Registrarse</a>
          	</li>
		</c:otherwise>		
       </c:choose>
       </ul>
      </div>
    </div>
  	</nav>
  	
  	<div class="container-fluid">
  	<div class="row">
  	<div class="mx-auto bg-info p-5 rounded mb-4">
  	<h1 class="text-white">Login: </h1>
  	<hr />
	<form class="text-white" action="auth" method="post">
		<p>
			<label for="email">Correo electronico</label>
		</p>
		<p>
			<input type="email" name="email" placeholder="tucorreo@gmail.com" required="required" />
		</p>
		<p>
			<label for="password">Contraseña</label>
		</p>
		<p>
			<input type="password" name="password" required="required" />
		</p>
		<p>
			<input class="btn btn-success" type="submit" value="Iniciar sesion" />
		</p>
	</form>
	<c:choose>
		<c:when test="${param.status == 1}">
			<p class="text-danger">Las credenciales del usuario no son validas</p>
		</c:when>
		<c:when test="${param.status == 2}">
			<p class="text-danger">Error del sistema, contacte con el administrador</p>
		</c:when>
	</c:choose>
	</div>
	</div>
	</div>
	<footer class="py-3 bg-dark mb-0">
    <div class="container">
      <p class="m-0 text-center text-white">Despliegue de aplicaciones Web 2ºDAW</p>
    </div>
  	</footer>
</body>
</html>
