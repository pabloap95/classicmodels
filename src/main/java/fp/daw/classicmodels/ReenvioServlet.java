package fp.daw.classicmodels;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.mail.MessagingException;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;



@WebServlet("/reenvio")

public class ReenvioServlet extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		String email = request.getParameter("email");
		if (session.getAttribute("usuario") != null)
			response.sendRedirect("catalogo.jsp");
		else {
			Connection con = null;
			PreparedStatement stm = null;
			ResultSet rs = null;

			try {
				Context context = new InitialContext();
				DataSource ds = (DataSource) context.lookup("java:comp/env/jdbc/classicmodels");
				con = ds.getConnection();
				stm = con.prepareStatement("select confirmationCode from signups where customerEmail = ?");
				stm.setString(1, email);
				rs = stm.executeQuery();
				rs.next();
				String cc = rs.getString(1);
				MailService.sendConfirmationMessage(email, cc);
			} catch (SQLException | NamingException | MessagingException e) {
			} finally {
				if (rs != null)
					try {
						rs.close();
					} catch (SQLException e) {
					}
				if (stm != null)
					try {
						stm.close();
					} catch (SQLException e) {
					}
				if (con != null)
					try {
						con.close();
					} catch (SQLException e) {
					}
			}
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}


}
