package fp.daw.classicmodels;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;


import javax.mail.MessagingException;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

@WebServlet("/registro")
public class SignupServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		if (session.getAttribute("usuario") != null)
			response.sendRedirect("catalogo");
		else {
			String nombre = request.getParameter("nombre");
			String apellidos = request.getParameter("apellidos");
			String email = request.getParameter("email");
			String password = request.getParameter("password");
			if (nombre == null || apellidos == null || email == null || password == null)
				response.sendRedirect("registro.jsp");
			else {
				switch (signup(nombre, apellidos, email, password)) {
				case 0:
					response.sendRedirect("postregistro.jsp?email=" + email);
					break;
				case 1:
					response.sendRedirect("registro.jsp?status=1");
					break;
				case 2:
					response.sendRedirect("registro.jsp?status=2");
				}
			}
		}
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

	private int signup(String firstName, String lastName, String email, String password) {
		Connection con = null;
		PreparedStatement stm = null;
		int status = 0;
		try {
			Context context = new InitialContext();
			DataSource ds = (DataSource) context.lookup("java:comp/env/jdbc/classicmodels");
			con = ds.getConnection();
			stm = con.prepareStatement("insert into signups (firstName, lastName, customerEmail, "
					+ "confirmationCode, signupDate, password) values (?, ?, ?, ?, ?, ?)");
			stm.setString(1, firstName);
			stm.setString(2, lastName);
			stm.setString(3, email);
			String confirmationCode = HashUtils.getBase64Digest(email);
			stm.setString(4, confirmationCode);
			stm.setDate(5, new Date(System.currentTimeMillis()));
			stm.setString(6, HashUtils.getBase64Hash(password));
			stm.execute();
			MailService.sendConfirmationMessage(email, confirmationCode);
		} catch (SQLException e) {
			status = e.getErrorCode() == 1062 ? 1 : 2;
		} catch (InvalidKeySpecException | NamingException | NoSuchAlgorithmException | MessagingException e) {
			status = 2;
		} finally {
			if (stm != null)
				try {
					stm.close();
				} catch (SQLException e) {
				}
			if (con != null)
				try {
					con.close();
				} catch (SQLException e) {
				}
		}
		return status;
	}

}

